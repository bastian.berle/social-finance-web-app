# Social Finance Web App

## Inhaltsverzeichnis
- [Einleitung](#Einleitung)
- [Vorstellung & Einrichtung](#Vorstellung-&-Einrichtung)
- [Installation](#Installation)
- [Technischer Aufbau](#Technischer-Aufbau)

<hr>

## Einleitung
Coin2Gether ist genau diese Social-Finance-Plattform. Über die Webseite können die Mitglieder ihre Trades oder Gedanken posten und ihr Portfolio veröffentlichen. 

**Zurück zum** [Inhaltsverzeichnis](#Inhaltsverzeichnis)

<hr>

## Installation
Im gleichem Verzeichnis, wo sich die Docker-Compose-Datei befindet, öffnet man ein Terminal und gibt den Befehl `docker compose up` ein. Als Prerequisite sollte Docker installiert sein.

**Zurück zum** [Inhaltsverzeichnis](#Inhaltsverzeichnis)

<hr>

## Vorstellung & Einrichtung

### Datenbank
Um über pgAdmin auf die Datenbank zugreifen zu können, müssen zwei Schritte durchgeführt werden:
1. pgAdmin öffnen und sich dort anmelden
2. in pgAdmin eine Verbindung zur Datenbank aufbauen

#### Schritt 1
- [pgAdmin](http://localhost:8080/)
- Nutzername: admin@linuxhint.com
- Passwort: secret

#### Schritt 2
Die Server-Einstellungen lassen sich wie folgt öffnen:
![Pgadmin-open-connection](Stuff/pgadmin_open_connection.png)

Folgende Werte in die Reiter "General" und "Connection" eingeben:
- Name: postgres
- Host: db
- Port: 5432
- Maintenance database: postgres
- username: root
- password: 1234

![DB-connection](Stuff/db_connection.png)

Die Tabellen befinden sich unter:
![Tables](Stuff/tables.png)


### Swagger
[Swagger](http://localhost:5000/v1/)

#### Beispiellogin
Um Funktionen über Swagger zu testen, benötigt man einen validen Token. Diesen erhält man in dem man sich einloggt. 
- Email: jeffbezos@gmail.com
- Passwort: Passwort123!

Um die Funktion zu testen, klickt man auf "Try it out!". Das Eingabefeld lässt sich nun bearbeitet. Dort ersetzt man "string" durch die Email und das Passwort und klickt anschließend auf "Execute". In dem Response fenster steht nun der Token. Dieser besteht aus dem Wort "Bearer" und einer Reihe von Chars.
- Beispiel: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MjgzMjkzNzgsImlhdCI6MTYyODI0Mjk3OCwic3ViIjoiamVmZmJlem9zQGdtYWlsLmNvbSJ9.veBdJrGokd6YkWvnOOfPzbadpLy--h02S4mCCyTXusM

Diesen Token gibt man nun, wenn erforderlich, ein.

![Login-Swagger](Stuff/swagger.png)

### Flask Monitoring-Dashboard
- [Monitoring Dashboard](http://localhost:5000/dashboard)
- [Link zur offiziellen Dokumentation](https://flask-monitoringdashboard.readthedocs.io/en/latest/)
- Nutzername: admin
- Passwort: admin

Das Monitoring-Dashboard kann intern eingesetzt werden, um die API-Endpunkte zu tracken und zu überwachen. Dadruch können bspw. stark ausgelastet Endpunkte aufgeteilt werden und kaum ausgelastete Endpunkte, wenn natürlich möglich, zusammenzufassen.  

#### Dashboard Login
![Dashboard-Login](Stuff/dashboard_login.png)

#### Dashboard Homepage
![Dashboard-Homepage](Stuff/dashboard_home.png)

### Webapp
- [Coin2Gether-Webapp](http://localhost/)
#### Coole Dinge, die man auf Coin2Gether ausprobieren sollte:
1. Neues Konto registrieren (Daten werden aktuell lokal gespeichert)
2. Neuen Blogpost erstellen
3. Einem Nutzer folgen (idealerweise ChefBezos, da dieser bereits mehrere Posts veröffentlicht hat) (über das Suchicon nach Nutzern suchen)
4. "MyFeed" testen
5. Portfolio anlegen (dafür auf die eigene Profilseite --> C2G-Icon in der linken Ecke klicken; Klick auf dem Stift neben Portfolio)
6. Trade veröffentlichen
7. Profil updaten (Zahnrad neben dem eigenen Namen auf der Profilseite)
8. in den Dark- oder Light-Modus wechseln
9. Sprache auf Englisch oder Deutsch wechseln

**Zurück zum** [Inhaltsverzeichnis](#Inhaltsverzeichnis)
<hr>

## Technischer Aufbau
#### ER-Diagramm
![ER-Diagram](Stuff/er_test.svg)
#### BMPN-Ablaufplan fürs Erstellen eines Trades
![BPMN-Ablauf](Stuff/diagram.svg)
#### Technischer Aufbau des Projektes
![Technischer Aufbau](Stuff/Overview.png)
**Zurück zum** [Inhaltsverzeichnis](#Inhaltsverzeichnis)