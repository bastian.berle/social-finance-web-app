export interface Currency {
    id?: number,
    name: string,
    acronym: string,
    currency_type: string,
    symbol?: string,
    disabled?: boolean
}