from manage import app
from app import db
from app.model import *
from app.service import auth_service, user_service
from datetime import datetime

def generate_platforms():
    instagram = SocialMediaPlatform(platform_name = "Instagram", platform_specific_link="https://www.instagram.com/")
    facebook = SocialMediaPlatform(platform_name = "Facebook", platform_specific_link="https://www.facebook.com/")
    twitter = SocialMediaPlatform(platform_name = "Twitter", platform_specific_link="https://www.twitter.com/")
    linkedin = SocialMediaPlatform(platform_name = "LinkedIn", platform_specific_link="https://www.linkedin.com/in/")
    tiktok = SocialMediaPlatform(platform_name = "TikTok", platform_specific_link="https://www.tiktok.com/@")
    db.session.add(instagram)
    db.session.add(facebook)
    db.session.add(twitter)
    db.session.add(linkedin)
    db.session.add(tiktok)
    db.session.commit()

def add_user_account():
    user = User(username="basti", email="basti@berle.de")
    db.session.add(user)
    db.session.commit()
    facebook = SocialMediaPlatform(platform_name = "Facebook", platform_specific_link="www.facebook.com/")
    db.session.add(facebook)
    db.session.commit()
    account = SocialMediaAccount(username="basti")
    account.platform = SocialMediaPlatform(platform_name = "Instagram", platform_specific_link="www.instagram.com/")
    user.accounts.append(account)
    db.session.commit()
    accs = user.accounts#.with_entities(SocialMediaPlatform.platform_name)
    print(accs[0].platform.platform_specific_link)
    print(accs[0].username)
    print(accs[0].platform.platform_name)
    for acc in accs:
        print(acc.platform.platform_specific_link + acc.username)


if __name__ == '__main__':
    with app.app_context():

        # add_user_account()
        # user = User.query.get(26)
        #print(user.username)
        # print(followers.query.filter(followers.c.followed_id == user.user_id))
        # for i in range(35):
        #     print(i, user.followed.filter(followers.c.followed_id == i).count(), user.followed.filter(followers.c.follower_id == i).count()) #gibt 1 zurück, wenn Person dieser folgt
        # print(user.followed.filter(followers.c.follower_id == user.user_id).count()) #Spalte 1, Leuten, denen ich folge
        # a = User.query.join(followers, (followers.c.followed_id == User.user_id)).filter(followers.c.follower_id == user.user_id).all()
        # print(user.my_posts().all().with_entities(Post.title))
        # a = Post.query.join(User, (User.user_id == Post.user_id)).filter(User.username == "susan").all()


        # user = User.query.first()
        # p = Post(title="abc", description="u", user=user)
        # db.session.add(p)
        # db.session.commit()

        # print(Post.get_all_posts())

        # token = auth_service.create_token("david@example.com")
        # print(token)
        # print(auth_service.verify_token(token))
        # print(auth_service.get_user_from_token(token).email)
       
        # user_service.register(username="BastiAwesomeX4", email="bastian.berle1114@gmail.com", lastName="Berle", firstName="Basti", password="ABCdie Katze liegt im Schnee", birthdate=str(datetime.today()))
        # token = user_service.login(email="bastian.berle@gmail.com", password="ABCdie Katze liegt im Schnee")
        # print(token)
        # print(auth_service.verify_token(token))
        # print(Post.query.with_entities(Post.title, Post.date, Post.description, Post.user_id).all())
        generate_platforms()

      