from app import db
from app.model import *
from app.service.user_service import *
import datetime
import random
from sqlalchemy import text

def saveAllToDB(data):
    db.session.add_all(data)
    db.session.commit()

def createPlatforms():
    twitter = SocialMediaPlatform(platform_name = "Twitter", platform_specific_link="https://www.twitter.com/")
    instagram = SocialMediaPlatform(platform_name = "Instagram", platform_specific_link="https://www.instagram.com/")
    facebook = SocialMediaPlatform(platform_name = "Facebook", platform_specific_link="https://www.facebook.com/")
    linkedin = SocialMediaPlatform(platform_name = "LinkedIn", platform_specific_link="https://www.linkedin.com/in/")
    tiktok = SocialMediaPlatform(platform_name = "TikTok", platform_specific_link="https://www.tiktok.com/@")
    db.session.add(twitter)
    db.session.add(instagram)
    db.session.add(facebook)
    db.session.add(linkedin)
    db.session.add(tiktok)
    db.session.commit()
    print('added: platforms')

def createCurrencies():
    currencies = []
    currencies.append(Currency(name='Bitcoin', acronym='BTC', currency_type='crypto', symbol="฿"))
    currencies.append(Currency(name='Ethereum', acronym='ETH', currency_type='crypto', symbol="Ξ"))
    currencies.append(Currency(name='Euro', acronym='EUR', currency_type='fiat', symbol='€'))
    currencies.append(Currency(name='US-Dollar', acronym='USD', currency_type='fiat', symbol="$"))
    currencies.append(Currency(name='Canadian-Dollar', acronym='CAD', currency_type='fiat', symbol="$"))
    currencies.append(Currency(name='Bitcoin Cash', acronym='BCH', currency_type='crypto', symbol=""))
    currencies.append(Currency(name='Dogecoin', acronym='DOGE', currency_type='crypto', symbol="Ɖ"))
    currencies.append(Currency(name='Litecoin', acronym='LTC', currency_type='crypto', symbol="Ł"))
    currencies.append(Currency(name='Yen', acronym='JPY', currency_type='fiat', symbol='¥'))
    currencies.append(Currency(name='Yuán', acronym='CNY', currency_type='fiat', symbol='¥'))
    saveAllToDB(currencies)
    print('added: currencies')

def createUsers():
    register(username="ChefBesos", email="jeffbezos@gmail.com", lastName="Besos", firstName="Chef", password="Passwort123!", birthdate=str(datetime.datetime.today() - datetime.timedelta(days=20*365)))
    register(username="EronMisk", email="eronmisk@gmail.com", lastName="Misk", firstName="Eron", password="Passwort123!", birthdate=str(datetime.datetime.today() - datetime.timedelta(days=21*365)))
    register(username="billyShorts", email="billgates@gmail.com", lastName="Gates", firstName="Jill I'm Single", password="Passwort123!", birthdate=str(datetime.datetime.today() - datetime.timedelta(days=22*365)))
    register(username="Max", email="max@gmail.com", lastName="Mustermann", firstName="Max", password="Passwort123!", birthdate=str(datetime.datetime.today() - datetime.timedelta(days=23*365)))
    register(username="Herbert", email="herbert@gmail.com", lastName="Herbert", firstName="Herbert", password="Passwort123!", birthdate=str(datetime.datetime.today() - datetime.timedelta(days=90*365)))
    print('added: users')

def createSocialMediaAccounts():
    platform = SocialMediaPlatform.query.first()
    user = User.query.first()
    account = []
    account.append(SocialMediaAccount(platform=platform, user=user, username='elonmusk'))
    saveAllToDB(account)
    print('added: accounts')    

def createFollowers():
    users = User.query.all()
    users[0].followed.append(users[1])
    users[0].followed.append(users[2])
    users[0].followed.append(users[3])
    users[0].followed.append(users[4])
    users[1].followed.append(users[0])
    users[2].followed.append(users[0])
    users[3].followed.append(users[0])
    db.session.commit()
    print('added: followers')  

def createPortfolios():
    users = User.query.all()
    for user in users:
        user.portfolio = Portfolio()
    saveAllToDB(users)
    print('added: portfolios')

def createComponents():
    currencies = Currency.query.all()
    portfolios = Portfolio.query.all()
    components = []
    for portfolio in portfolios:
        for currency in currencies:
            if (bool(random.getrandbits(1))): # random bool deciding if the currency will be within the portfolio
                components.append(Component(portfolio=portfolio, currency=currency, amount=round(random.uniform(0, 1_000_000), 2)))
    saveAllToDB(components)
    print('added: components')

def createPosts():
    users = User.query.all()
    p1 = Post(title="Stonks only go up", description="post from john", user=users[0], date=datetime.datetime.today() + datetime.timedelta(days=1))
    p2 = Post(title="This platform is the holy grail of trading", description="Thanks to C2G I was able to quit university.", user=users[0], date=datetime.datetime.today() - datetime.timedelta(days=4))
    p3 = Post(title="The detail view of posts is great", description="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", user=users[1], date=datetime.datetime.today() - datetime.timedelta(days=3))
    p4 = Post(title="Small post", description=".", user=users[2], date=datetime.datetime.today() - datetime.timedelta(days=10))
    saveAllToDB([p1, p2, p3])
    saveAllToDB([p4])
    print('added: posts')

def createTrades():
    users = User.query.all()
    currencies = Currency.query.all()
    trades = []
    trades.append(
        Trade(
            publish_date=datetime.datetime.now(),
            start_date=datetime.datetime(2021, 3, 1),
            end_date=datetime.datetime(2021, 4, 1),
            motivation='Bitcoin is going high',
            description='Trade now, it is important! Dont lose money on waiting!',
            expected_change=random.randint(0, 250),
            percentage_trade=20,
            author=users[0],
            debit_currency=currencies[0],
            debit_amount=round(random.uniform(0, 1_000), 2),
            credit_currency=currencies[1],
            credit_amount=round(random.uniform(0, 1_000), 2),
        )
    )
    trades.append(
        Trade(
            publish_date=datetime.datetime.now(),
            start_date=datetime.datetime(2021, 3, 1),
            end_date=datetime.datetime(2021, 4, 1),
            motivation='This is my motivation.',
            description='Trade now, it is important! GOGOGOGOGOGOGOGOG!!!',
            expected_change=random.randint(0, 250),
            percentage_trade=20,
            author=users[1],
            debit_currency=currencies[0],
            debit_amount=round(random.uniform(0, 1_000), 2),
            credit_currency=currencies[1],
            credit_amount=round(random.uniform(0, 1_000), 2),
        )
    )
    trades.append(
        Trade(
            publish_date=datetime.datetime.now(),
            start_date=datetime.datetime(2021, 3, 1),
            end_date=datetime.datetime(2021, 4, 1),
            motivation='Stop the Hedgefunds. All in Dodgecoin',
            description='',
            expected_change=random.randint(0, 250),
            percentage_trade=20,
            author=users[1],
            debit_currency=currencies[6],
            debit_amount=round(random.uniform(0, 1_000), 2),
            credit_currency=currencies[1],
            credit_amount=round(random.uniform(0, 1_000), 2),
        )
    )
    saveAllToDB(trades)
    print('added: trades')

def create_view():
    last_posts = text("CREATE VIEW last_10_posts AS SELECT * FROM posts LIMIT 10;")
    db.engine.execute(last_posts)
    last_trades = text("CREATE VIEW last_10_trades AS SELECT * FROM trades LIMIT 10;")
    db.engine.execute(last_trades)
    print('added: view')

def create_procedure():
    new_follow = text("CREATE PROCEDURE new_follow(src_user integer, tgt_user integer) LANGUAGE SQL AS $$ INSERT INTO followers VALUES (src_user, tgt_user); $$;")
    delete_follow = text("CREATE PROCEDURE delete_follow(src_user integer, tgt_user integer) LANGUAGE SQL AS $$ DELETE FROM followers WHERE follower_id = src_user AND followed_id = tgt_user; $$;")
    db.engine.execute(new_follow)
    db.engine.execute(delete_follow)
    print('added: procedure')

def create_procedure_and_trigger():
    sequence = text("""CREATE SEQUENCE table_follow_log_id_seq;""")
    table_follow_log = text("""CREATE TABLE follow_log ( log_id INTEGER NOT NULL DEFAULT nextval('table_follow_log_id_seq'), follower_id INTEGER NOT NULL, followed_id INTEGER NOT NULL, date timestamp without time zone NOT NULL, action_type varchar NOT NULL, PRIMARY KEY (log_id), FOREIGN KEY (followed_id) REFERENCES public.users (user_id) MATCH SIMPLE, FOREIGN KEY (follower_id) REFERENCES public.users (user_id) MATCH SIMPLE );""")
    procedure_new_follow = text("""CREATE OR REPLACE FUNCTION trigger_new_follow() RETURNS trigger AS $$ BEGIN INSERT INTO follow_log (follower_id, followed_id, date, action_type) VALUES (NEW.follower_id, NEW.followed_id, now(), 'Add'); RETURN NEW; END; $$ LANGUAGE 'plpgsql';""")
    procedure_delete_follow = text("""CREATE OR REPLACE FUNCTION trigger_delete_follow() RETURNS trigger AS $$ BEGIN INSERT INTO follow_log (follower_id, followed_id, date, action_type) VALUES (OLD.follower_id, OLD.followed_id, now(), 'Delete'); RETURN NEW; END; $$ LANGUAGE 'plpgsql';""")
    trigger_new_follow = text("""CREATE TRIGGER insert_follow_trigger AFTER INSERT ON followers FOR EACH ROW EXECUTE PROCEDURE trigger_new_follow();""")
    trigger_delete_follow = text("""CREATE TRIGGER delete_follow_trigger AFTER DELETE ON followers FOR EACH ROW EXECUTE PROCEDURE trigger_delete_follow();""")
    db.engine.execute(sequence)
    db.engine.execute(table_follow_log)
    db.engine.execute(procedure_new_follow)
    db.engine.execute(procedure_delete_follow)
    db.engine.execute(trigger_new_follow)
    db.engine.execute(trigger_delete_follow)
    print('added: procedure & trigger')

    

def generate():
    create_procedure()
    create_procedure_and_trigger()
    createPlatforms()
    createCurrencies()
    createUsers()
    createSocialMediaAccounts()
    # createPortfolios()
    createComponents()
    # createTrades()
    createFollowers()
    createPosts()
    create_view()